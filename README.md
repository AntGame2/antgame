# Ant Game

Here is some general information regarding the project:

## Assigned Tasks
#### Allen (gs270):
* Ant
* Brain
* File I/O: Parse brain, 

#### Michael Sledge:
* Display
* File I/O: Parse world

#### Alex (ArgusRaven):
* Game Handler
* World

#### To Man (ttt31):
* Cell
* RNG
* Combat

#### Gloria:
* File I/O: dumpwriter

For an outline of what each module should do see [the google doc](https://docs.google.com/document/d/1W23g8T_HKApPiIWW12yJytlZmnT4CU9cuzz9t-ormdQ).

## Coding Style Guidelines

Please try to stick to the following guidlines:

* Use "speaking" (i.e. self explanatory) names (variables, classes, and methods).
* Use constants (static final variables) instead of "Magic numbers": Whenever a number has a special meaning other than a simple counter, create a constant that explains the meaning of the number and use only the constant.
* Use comments to explain why something was coded in a certain way, not to explain how something was coded, or what the code does.
e.g.  don't write `x += 1; // add one to x`; but do write `x += 1; // move right one place` if it's not obvious
* Use JavaDoc to document your code.