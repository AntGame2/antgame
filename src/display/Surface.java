package display;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import model.ant.Ant;
import model.world.Cell;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.Point;
import java.io.IOException;
import java.util.Vector;

import model.ant.Ant.AntColour;


/**
 * Class for rendering ant world to be shown in GUI
 * @author Michael Sledge
 *
 */
@SuppressWarnings("serial")
class Surface extends JPanel {

    private static final int ZOOM_LEVELS[] = {2,4,8,16};

    private int zoomLevel = 2;
    private int originX = 0, originY = 0;

    private Image[] mapImage = new Image[ZOOM_LEVELS.length];

    private boolean resourcesLoaded = false;
    private static final int ANT_COLOUR_BLACK = 0;
    private static final int ANT_COLOUR_RED   = 1;
    private static final int ANT_COLOUR_WHITE = 2;
    private Image[][] antImages = new Image[ZOOM_LEVELS.length][3];

    private DisplayData dispData;

    public Surface(){
        loadResources();
    }

    /**
     * Zoom in or out by the given amount
     * @param amount the 'distance' to zoom in, negative to zoom out
     */
    void updateZoom(int amount){
        if (amount == 0) return;


        if (amount > 0){
            if (zoomLevel > 0){
                // zoom out
                --zoomLevel;
                // zoom in on centre of screen
                originX /= 2;
                originY /= 2;
                originX -= this.getWidth()/4;
                originY -= this.getHeight()/4;
                repaint();}
        }else{ // amount < 0
            if (zoomLevel < ZOOM_LEVELS.length - 1){
                // zoom in
                ++zoomLevel;
                // zoom in on centre of screen
                originX *= 2;
                originY *= 2;
                originX += this.getWidth()/2;
                originY += this.getHeight()/2;
                repaint();}
        }
    }

    /**
     * Move the displayed image.
     * Positive values for h and v move right and down respectively
     * @param h the number of pixels to move horizontally
     * @param v the number of pixels to move vertically
     */
    void translate(int h, int v){
        originX -= h;
        originY -= v;

        // bounds checking
        if(mapImage[zoomLevel] != null){
            // prevent image going beyond left edge
            if ((-originX) + mapImage[zoomLevel].getWidth(null) < 0){
                originX = mapImage[zoomLevel].getWidth(null);
            }
            // prevent image going beyond top edge
            if ((-originY) + mapImage[zoomLevel].getHeight(null) < 0){
                originY = mapImage[zoomLevel].getHeight(null);
            }
            // prevent image going beyond right edge
            if ((-originX) > getWidth()){
                originX = -getWidth();
            }
            // prevent image going beyond bottom edge
            if ((-originY) > getHeight()){
                originY = -getHeight();
            }
        }

        repaint();
    }

    /**
     * Loads the required resources.
     * Does nothing if resources are already loaded
     */
    void loadResources(){
        if (resourcesLoaded) return;
        try{
            for (int i = 0; i < ZOOM_LEVELS.length; ++i){
                int j = 0;
                for (char colour : new char[]{'b', 'r', 'w'}){
                    antImages[i][j] = ImageIO.read(Surface.class.getResource("/display/images/ant_" + (ZOOM_LEVELS[i]*4) + "_" + colour + ".png"));
                    ++j;
                }
            }
            resourcesLoaded = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the data to be displayed
     * @param _dispData A Display Data object containing the information needed for rendering
     */
    public void setDisplayData(DisplayData _dispData){
        dispData = _dispData;
        onMapUpdate();
    }

    /**
     * Call whenever the information in the DisplayData has changed and needs to be re-rendered e.g. after each round
     */
    public void onWorldUpdate(){
        repaint();
    }
    
    /**
     * Called whenever the map has changed (only when cell types have changed, not when food or ants move)
     */
    public void onMapUpdate(){
        Vector<Thread> threads = new Vector<>();
        for (int i = 0; i < ZOOM_LEVELS.length; ++i){
            final int zl = i;
            Thread t = new Thread((Runnable)()->{redraw(zl);});
            threads.add(t);
            t.start();
        }
        Thread redrawThread = new Thread((Runnable)()->{
            for(Thread t : threads){
                try{
                    t.join();
                }catch (Exception e){}
            }
            onWorldUpdate();
        });
        redrawThread.start();
    }

    /**
     * redraws the map at the given zoom level
     * @param zoomLevel the zoom level to redraw
     */
    private void redraw(int zoomLevel){

        // nothing to render
        if (dispData == null)
            return;

        // no map to render
        if (dispData.getMap() == null)
            return;

        // not a valid zoom level
        if (zoomLevel < 0 || zoomLevel >= ZOOM_LEVELS.length)
            return;

        int originXImg = ZOOM_LEVELS[zoomLevel] * 2;
        int originYImg = ZOOM_LEVELS[zoomLevel] * 2;
        int imgScale = ZOOM_LEVELS[zoomLevel];

        int mapHeight = dispData.getMap().getHeight();
        int mapWidth = dispData.getMap().getWidth();
        int imgWidth = imgScale * 4 * mapWidth + imgScale * 2;
        int imgHeight = imgScale * 3 * mapHeight + imgScale * 2;

        BufferedImage img = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = img.createGraphics();

        g.setColor(Color.CYAN);
        g.fillRect(0, 0, imgWidth, imgHeight);

        /*
         * Map rendering
         */

        // Assume most cells are clear => yellow
        g.setColor(Color.YELLOW);
        g.fillRect(originXImg, originYImg, (mapWidth - 1) * imgScale * 4, (mapHeight - 1) * imgScale * 3);

        Hexagon baseHex = new Hexagon().scale(ZOOM_LEVELS[zoomLevel]);

        for (int y = 0; y < mapHeight; ++y){

            for (int x = 0; x < mapWidth; ++x){

                Point coords = mapXYtoRealXY(x,y, zoomLevel);

                Color col;
                Cell cell = dispData.getMap().getCell(x, y);
                switch (cell.type){
                    case CLEAR:
                        if(x != 0 && x != mapWidth-1 && y != 0 && y != mapHeight-1){
                            // outline but don't fill clear cells unless they are on the edge
                            renderEasyHex(baseHex.translate(coords.x, coords.y),
                                    Color.BLACK, (Graphics2D) g, false);
                            continue;
                        }else{
                            col = Color.YELLOW;
                        }
                        break;
                    case ROCKY:
                        col = Color.GRAY;
                        break;
                    case ANTHILL_RED:
                        col = Color.RED;
                        break;
                    case ANTHILL_BLACK:
                        col = Color.BLACK;
                        break;
                    default:
                        col = Color.MAGENTA;
                }
                // fill
                renderEasyHex(baseHex.translate(coords.x, coords.y),
                        col, (Graphics2D) g, true);
                // outline
                renderEasyHex(baseHex.translate(coords.x, coords.y),
                        Color.BLACK, (Graphics2D) g, false);
            }
        }


        mapImage[zoomLevel] = img;
    }

    /**
     * Helper method for converting cell coordinates to pixel coordinates (with the centre of cell 0,0 as the origin)
     * @param x horizontal coordinate of cell
     * @param y vertical coordinate of cell
     * @param zoomLevel zoom level to calculate with regards to
     * @return pixel coordinates with the centre of cell 0,0 as the origin
     */
    private Point mapXYtoRealXY(int x, int y, int zoomLevel){
        int _x = x * ZOOM_LEVELS[zoomLevel] * 4 + ZOOM_LEVELS[zoomLevel] * 2 + (y % 2 == 0 ? 0 : ZOOM_LEVELS[zoomLevel] * 2 );
        int _y = y * ZOOM_LEVELS[zoomLevel] * 3 + ZOOM_LEVELS[zoomLevel] * 2;
        return new Point(_x, _y);
    }

    /**
     * Helper method for converting cell coordinates to pixel coordinates (with the centre of cell 0,0 as the origin)
     * @param p coordinates of cell
     * @param zoomLevel zoom level to calculate with regards to
     * @return pixel coordinates with the centre of cell 0,0 as the origin
     */
    private Point mapXYtoRealXY(Point p, int zoomLevel){
        return mapXYtoRealXY(p.x, p.y, zoomLevel);
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        // nothing to display
        if (dispData == null)
            return;

        fillBG((Graphics2D) g);

        // height and width of this JComponent
        int width = getWidth(), height = getHeight();

        g.drawImage(mapImage[zoomLevel], 0, 0, width, height,
                originX, originY, originX + width, originY + height, null);
        
        /*
         * Food rendering (smallest scale)
         * at the smallest scale (most zoomed out) food is rendered as green hexes under the ants
         */
        if (dispData.getMap() != null){
            if (zoomLevel == 0){ // otherwise render food number later
    
                int mapHeight = dispData.getMap().getHeight();
                int mapWidth = dispData.getMap().getWidth();

                Hexagon baseHex = new Hexagon().scale(ZOOM_LEVELS[zoomLevel]);
        
        
                for (int y = 0; y < mapHeight; ++y){
        
                    for (int x = 0; x < mapWidth; ++x){
        
                        Point coords = mapXYtoRealXY(x,y, zoomLevel);
                        coords.translate(-originX, -originY);
                        
                        // don't render food info off screen
                        if (coords.x > width + ZOOM_LEVELS[zoomLevel] *  2|| coords.x < 0 - ZOOM_LEVELS[zoomLevel] *  2 ||
                                coords.y > height + ZOOM_LEVELS[zoomLevel] *  2 || coords.y < 0 - ZOOM_LEVELS[zoomLevel] *  2)
                            continue;
        
                        Cell cell = dispData.getMap().getCell(x, y);
                        if(cell.foodCount() == 0)
                            continue;
                        // only render food > 0
                        switch (cell.type){
                            case CLEAR:
                            case ANTHILL_RED:
                            case ANTHILL_BLACK:
                                // fill
                                renderEasyHex(baseHex.translate(coords.x, coords.y),
                                        Color.GREEN.darker(), (Graphics2D) g, true);
                                // outline
                                renderEasyHex(baseHex.translate(coords.x, coords.y),
                                        Color.BLACK, (Graphics2D) g, false);
                                break;
                            default:
                                continue;
                        }
                    }
                }
            }
        }

        /*
         * Ants rendering
         */

        Ant[] ants = dispData.getAnts();
        if (ants != null){
            for (Ant a : ants){
                if (!a.isAlive()) continue;
                Point mapPos = a.getPosition();
                Point pos = mapXYtoRealXY(mapPos, zoomLevel);
                pos.translate(-originX, -originY);
    
                // don't render ants off screen
                if (pos.x > width + ZOOM_LEVELS[zoomLevel] *  2|| pos.x < 0 - ZOOM_LEVELS[zoomLevel] *  2 ||
                        pos.y > height + ZOOM_LEVELS[zoomLevel] *  2 || pos.y < 0 - ZOOM_LEVELS[zoomLevel] *  2)
                    continue;
    
                if (a.getColour() == AntColour.BLACK){ // black, apparently. why is this not an Enum or int again? (Alex) Don't ask me, but I changed it when I saw it.
                    if (dispData.getMap().getCell(mapPos.x, mapPos.y).type == Cell.Type.ANTHILL_BLACK){
                        // can't see black ants on the black anthill
                        renderAnt(pos, a.getDirection(), ANT_COLOUR_WHITE, zoomLevel, (Graphics2D) g);
                    }else
                        renderAnt(pos, a.getDirection(), ANT_COLOUR_BLACK, zoomLevel, (Graphics2D) g);
    
                }else{ // not black so it must be red
                    if (dispData.getMap().getCell(mapPos.x, mapPos.y).type == Cell.Type.ANTHILL_RED){
                        // can't see red ants on the red anthill
                        renderAnt(pos, a.getDirection(), ANT_COLOUR_WHITE, zoomLevel, (Graphics2D) g);
                    }else
                        renderAnt(pos, a.getDirection(), ANT_COLOUR_RED, zoomLevel, (Graphics2D) g);
                }
            }
        }
        
        /*
         * Food rendering
         * at the all but the smallest scale (most zoomed out) food is rendered as green digits over the ants
         */
        if (dispData.getMap() != null && zoomLevel != 0){
    
            int mapHeight = dispData.getMap().getHeight();
            int mapWidth = dispData.getMap().getWidth();
            
            g.setColor(Color.GREEN.darker());
            g.setFont(new Font("Monospaced", Font.BOLD, ZOOM_LEVELS[zoomLevel] * 3));
    
    
            for (int y = 0; y < mapHeight; ++y){
    
                for (int x = 0; x < mapWidth; ++x){
    
                    Point coords = mapXYtoRealXY(x,y, zoomLevel);
                    coords.translate(-originX, -originY);
                    coords.translate(-ZOOM_LEVELS[zoomLevel], ZOOM_LEVELS[zoomLevel]);// make text more centred
                    
                    // don't render food info off screen
                    if (coords.x > width + ZOOM_LEVELS[zoomLevel] *  2|| coords.x < 0 - ZOOM_LEVELS[zoomLevel] *  2 ||
                            coords.y > height + ZOOM_LEVELS[zoomLevel] *  2 || coords.y < 0 - ZOOM_LEVELS[zoomLevel] *  2)
                        continue;
    
                    Cell cell = dispData.getMap().getCell(x, y);
                    if(cell.foodCount() == 0)
                        continue;
                    // only render food > 0
                    switch (cell.type){
                        case CLEAR:
                        case ANTHILL_RED:
                        case ANTHILL_BLACK:
                            String number = ""+cell.foodCount();
                            g.drawString(number, coords.x, coords.y);
                            break;
                        default:
                            continue;
                    }
                }
            }
        }

        g.setColor(Color.BLACK);
        g.fillRect(width - 48, 0, 48, 28);
        g.setColor(Color.WHITE);
        g.setFont(new Font("Monospaced", Font.BOLD, 16));
        String number = String.format("%04d", dispData.scoreBlack);
        g.drawString(number, width - 44, 20);

        g.setColor(Color.RED);
        g.fillRect(0, 0, 48, 28);
        g.setColor(Color.WHITE);
        g.setFont(new Font("Monospaced", Font.BOLD, 16));
        number = String.format("%04d", dispData.scoreRed);
        g.drawString(number, 4, 20);
        
    }

    /**
     * helper for filling this JComponent with a colour
     * @param g Graphics2D to render to
     */
    private void fillBG(Graphics2D g){
        g.setColor(Color.CYAN);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    /**
     * Renders an image of an ant
     * @param pos position to render ant
     * @param direction direction the ant is facing. 0 = right, increasing clockwise
     * @param colour colour ant to render (use ANT_COLOUR_ constants)
     * @param zoomLevel zoom level to render with regards to
     * @param g Graphics2D to render to
     */
    private void renderAnt(Point pos, int direction, int colour, int zoomLevel, Graphics2D g){
        int imageRes = ZOOM_LEVELS[zoomLevel] * 4;
        AffineTransform at = new AffineTransform();
        at.translate(pos.x, pos.y);
        at.rotate(direction * Math.PI/3);
        at.translate(-imageRes/2, -imageRes/2);
        g.drawImage(antImages[zoomLevel][colour], at, null);
    }

    /**
     * Renders an approximation of a regular hexagon at the specified point with
     * the specified width and height
     * @param hexagon a Hexagon representing the size and position of the hexagon to be rendered
     * @param c The colour to fill or outline with
     * @param g the graphics object to render on
     * @param fill true to fill with colour c, false to outline with colour c
     */
    private void renderEasyHex(Hexagon hexagon, Color c, Graphics2D g, boolean fill){
        g.setColor(c);

        if (fill){
            g.fillPolygon(hexagon.xPoints, hexagon.yPoints, 6);
        }else{
            g.drawPolygon(hexagon.xPoints, hexagon.yPoints, 6);
        }
    }
}
