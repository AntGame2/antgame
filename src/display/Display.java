package display;

import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.JSlider;
import javax.swing.SwingWorker;

import static javax.swing.JOptionPane.showMessageDialog;
import model.world.GameHandler;

/**
 * The GUI class for the ant game, provides interface to upload world and brains and run and view simulation
 * @author Michael Sledge
 *
 */
@SuppressWarnings("serial")
public class Display extends JFrame{

    private static final String TITLE = "Ant Game";

    SimulationThread simT = null;
    int simulationSpeed = 50;
    boolean simRunning = false;
    static final Object simMonitor = new Object();

    private Surface surface;

    private GameHandler game;

    // buttons
    private JButton bLoadWorld;
    private JButton bLoadAntR;
    private JButton bLoadAntB;
    private JButton bStart;
    private JButton bRunSim;

    /**
     * Creates the GUI
     */
    public Display(){
        super();
        setMinimumSize(new Dimension(750, 320));
        setTitle(TITLE);

        Container contentPane = this.getContentPane();
        contentPane.setLayout(new BorderLayout());

        game = new GameHandler();
        surface = new Surface();

        surface.addMouseMotionListener(new MouseMotionListener(){
            private int mousePrevX = 0, mousePrevY = 0;

            @Override
            public void mouseDragged(MouseEvent e) {
                // move the image by the amount of distance the mouse was dragged
                surface.translate(e.getX() - mousePrevX, e.getY() - mousePrevY);

                mousePrevX = e.getX();
                mousePrevY = e.getY();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                // could probably do this on mouse down instead

                // required so we know the position of the mouse at the start of a drag
                // as mouseDragged is only called after the mouse has moved while being held down
                mousePrevX = e.getX();
                mousePrevY = e.getY();
            }

        });
        surface.addMouseWheelListener((e)-> {
            surface.updateZoom(e.getWheelRotation());
        });

        contentPane.add(surface, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());

        bLoadWorld = new JButton("Load World");
        bLoadWorld.addActionListener((e)->{
            final JFileChooser fc = new JFileChooser();
            int retVal = fc.showOpenDialog(this);
            if (retVal == JFileChooser.APPROVE_OPTION){
                try {
                    game.setMap(fileIO.parsers.MapParser.parseMap(fc.getSelectedFile()));
                    if (game.canStart()){
                        bStart.setEnabled(true);
                    }
                } catch (Exception e1) {
                    showMessage(e1.getMessage(), "Error parsing map", MessageType.ERROR);
                }
            }
        });
        buttonPanel.add(bLoadWorld);

        bLoadAntR = new JButton("Set Red Brain");
        bLoadAntR.addActionListener((e)->{
            final JFileChooser fc = new JFileChooser();
            int retVal = fc.showOpenDialog(this);
            if (retVal == JFileChooser.APPROVE_OPTION){
                try {
                    game.setAntBrainRed(fileIO.parsers.BrainParser.ParseBrain(fc.getSelectedFile().getAbsolutePath()));
                    if (game.canStart()){
                        bStart.setEnabled(true);
                    }
                } catch (Exception e1) {
                    showMessage(e1.getMessage(), "Error parsing brain", MessageType.ERROR);
                }
            }
        });
        buttonPanel.add(bLoadAntR);

        bLoadAntB = new JButton("Set Black Brain");
        bLoadAntB.addActionListener((e)->{
            final JFileChooser fc = new JFileChooser();
            int retVal = fc.showOpenDialog(this);
            if (retVal == JFileChooser.APPROVE_OPTION){
                try {
                    game.setAntBrainBlack(fileIO.parsers.BrainParser.ParseBrain(fc.getSelectedFile().getAbsolutePath()));
                    if (game.canStart()){
                        bStart.setEnabled(true);
                    }
                } catch (Exception e1) {
                    showMessage(e1.getMessage(), "Error parsing brain", MessageType.ERROR);
                }
            }
        });
        buttonPanel.add(bLoadAntB);

        bStart = new JButton("Start Game");
        bStart.addActionListener((e)->{
            game.startGame();
            surface.setDisplayData(game.getDisplayData());
            bRunSim.setEnabled(true);
            bStart.setEnabled(false);
            bLoadAntB.setEnabled(false);
            bLoadAntR.setEnabled(false);
            bLoadWorld.setEnabled(false);
        });
        bStart.setEnabled(false);
        buttonPanel.add(bStart);

        bRunSim = new JButton("Run simulation");
        bRunSim.addActionListener((e)->{
            if(simT == null){
                simRunning = true;
                simT = new SimulationThread();
                simT.execute();
                bRunSim.setText("Pause simulation");
            }else{
                if(simRunning){ // this is the pause button
                    simRunning = false;
                    bRunSim.setText("Run simulation");
                }else{ // this is the run button
                    simRunning = true;
                    synchronized (simMonitor){
                        simMonitor.notifyAll();}// let the simulation thread know it can start again
                    bRunSim.setText("Pause simulation");
                }
            }
        });
        bRunSim.setEnabled(false);
        buttonPanel.add(bRunSim);

        JSlider simSpeedSlider = new JSlider(JSlider.HORIZONTAL, 1, 100, 50);
        simSpeedSlider.addChangeListener((e)->{
            JSlider source = (JSlider)e.getSource();
            simulationSpeed = source.getValue();
        });
        simSpeedSlider.setMajorTickSpacing(20);
        simSpeedSlider.setMinorTickSpacing(5);
        buttonPanel.add(simSpeedSlider);

        contentPane.add(buttonPanel, BorderLayout.SOUTH);

        setSize(640, 480);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null); // centre on screen
    }

    public enum MessageType{ERROR(JOptionPane.ERROR_MESSAGE, "Error"),
        INFO(JOptionPane.INFORMATION_MESSAGE, "Information"),
        WARN(JOptionPane.WARNING_MESSAGE, "Warning"),
        PLAIN(JOptionPane.PLAIN_MESSAGE, "Message");
        private int _type;
        private String _string;
        private int type(){ return _type; }
        private String string(){ return _string; }
        private MessageType(int type, String string){
            _type = type;
            _string = string;
        }
    };

    /**
     * Show a message Dialogue
     * @param message the message to display
     * @param title the title of the message box
     * @param type type of message: ERROR, INFO, WARN or PLAIN (if null then PLAIN)
     */
    public void showMessage(String message, String title, MessageType type){
        if (type != null){
            if (message == null) message = type.string();
            showMessageDialog(this, message, title, type.type());
        }
        else{
            if (message == null) message = MessageType.PLAIN.string();
            showMessageDialog(this, message, title, MessageType.PLAIN.type());
        }
    }

    /**
     * Thread to run simulation asynchronously with display
     * @author Michael Sledge
     *
     */
    private class SimulationThread extends SwingWorker<Void, Void>{

        // the method that runs when the thread executes
        @Override
        protected Void doInBackground() throws Exception {
            while (!game.isFinished()){
                if (simRunning){
                    game.updateGame();
                    publish(null, null);
                    Thread.sleep((100 / simulationSpeed)-1); // -1 so that at max speed it doesn't sleep
                }else{
                    //Thread.sleep(100);
                    synchronized (simMonitor){
                        simMonitor.wait();}
                }
            }
            return null;
        }

        // runs from event dispatch thread when doInBackground() returns
        @Override
        protected void done(){
            surface.onWorldUpdate();
            //setup options for reset
            game.setAntBrainBlack(null);
            game.setAntBrainRed(null);
            game.setMap(null);
            bRunSim.setEnabled(false);
            bRunSim.setText("Run simulation");
            bStart.setEnabled(false);
            bLoadAntB.setEnabled(true);
            bLoadAntR.setEnabled(true);
            bLoadWorld.setEnabled(true);
            simRunning = false;
            simT = null;
        }

        // runs from event dispatch thread when publish() is called
        @Override
        protected void process(java.util.List<Void> lv){
            surface.onWorldUpdate();
        }
    }


}
