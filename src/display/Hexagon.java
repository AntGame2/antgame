package display;

/**
 * Wrapper for coordinates of hexagon shapes, used to render the world
 * @author Michael Sledge
 *
 */
class Hexagon {
	final int[] xPoints;
	final int[] yPoints;

	/**
	 * creates a new hexagon of the default size at coordinates 0,0
	 */
	public Hexagon(){
		// points top-left -> clockwise
		xPoints = new int[]{-2, 0, 2, 2, 0, -2};
		yPoints = new int[]{-1,-2, -1, 1, 2, 1};
	}

    /**
     * creates a new hexagon of the given size at coordinates 0,0
     * @param scale value to scale the hexagon by (relative to default size)
     */
	public Hexagon(int scale){
		// points top-left -> clockwise
		xPoints = new int[]{-2 * scale, 0, 2 * scale, 2 * scale, 0, -2 * scale};
		yPoints = new int[]{-scale,-2 * scale, -scale, scale, 2 * scale, scale};
	}

	private Hexagon(int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3, int y4, int y5, int y6){
		xPoints = new int[]{x1, x2, x3, x4, x5, x6};
		yPoints = new int[]{y1, y2, y3, y4, y5, y6};
	}

	/**
	 * returns a hexagon translated the given amount from this hexagons coordinates
	 * @param x horizontal translation
	 * @param y vertical translation
	 * @return hexagon at the given relative coordinates
	 */
	public Hexagon translate(int x, int y){
		return new Hexagon(xPoints[0] + x, xPoints[1] + x, xPoints[2] + x, xPoints[3] + x, xPoints[4] + x, xPoints[5] + x, 
						   yPoints[0] + y, yPoints[1] + y, yPoints[2] + y, yPoints[3] + y, yPoints[4] + y, yPoints[5] + y );
	}

	/**
     * returns a hexagon scaled the given amount from this hexagons
	 * @param scale the amount to scale by
     * @return hexagon at the given relative scale
	 */
	public Hexagon scale(double scale){
		return new Hexagon((int)(xPoints[0] * scale), (int)(xPoints[1] * scale), (int)(xPoints[2] * scale),
							(int)(xPoints[3] * scale), (int)(xPoints[4] * scale), (int)(xPoints[5] * scale), 
						   (int)(yPoints[0] * scale), (int)(yPoints[1] * scale), (int)(yPoints[2] * scale),
						    (int)(yPoints[3] * scale), (int)(yPoints[4] * scale), (int)(yPoints[5] * scale) );
	}

	/**
     * returns a hexagon scaled and translated by the given amount from this hexagons
     * @param scale the amount to scale by
     * @param x horizontal translation
     * @param y vertical translation
	 * @return hexagon at the given relative scale and coordinates
	 */
	public Hexagon scaleTranslate(double scale, int x, int y){
		return new Hexagon((int)(xPoints[0] * scale) + x, (int)(xPoints[1] * scale) + x, (int)(xPoints[2] * scale) + x,
							(int)(xPoints[3] * scale) + x, (int)(xPoints[4] * scale) + x, (int)(xPoints[5] * scale) + x, 
						   (int)(yPoints[0] * scale) + y, (int)(yPoints[1] * scale) + y, (int)(yPoints[2] * scale) + y,
						    (int)(yPoints[3] * scale) + y, (int)(yPoints[4] * scale) + y, (int)(yPoints[5] * scale) + y );
	}
}
