package display;

import model.ant.Ant;
import model.world.Map;

/**
 * A shared data structure for all ants and the map they are within
 * @author Alex
 */
public class DisplayData {
    //The map being used
    private Map map;
    //The ants at the current round
    private Ant[] ants;
	public int scoreRed = 0;
    public int scoreBlack = 0;

    /**
     * Set the ants 
     * @param _ants The ants after round processing
     */
    public void setAnts(Ant[] _ants){
        ants = _ants; 
    }
    
    /**
     * Set the current map used
     * @param _map The map being used
     */
    public void setMap(Map _map){
        map = _map;
    }
    
    /**
     * Return the ants 
     * @return The state of all ants at the end of the round
     */
    public Ant[] getAnts(){       
        return ants;
    }
    
    /**
     * Return the map
     * @return The map currently being used
     */
    public Map getMap(){
        return map;
    }
}
