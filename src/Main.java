import display.Display;
import fileIO.generators.WorldGenerator;
import fileIO.parsers.ParserException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.world.GameHandler;


public class Main {

    enum FunctionType {TOURNAMENT, SINGLE}

    public static void main(String[] args) throws IOException, ParserException {

        if (args.length == 0){
            runGUI();
            return;
        }

        boolean noGUI = false;
        String brainB = null;
        String brainR = null;
        String map = null;
        FunctionType type;
        int i = 1;
        if (args[0].equalsIgnoreCase("tournament")){
            type = FunctionType.TOURNAMENT;
            noGUI = true;
        }else if (args[0].equalsIgnoreCase("game")){
            type = FunctionType.SINGLE;
        }else if (args[0].equalsIgnoreCase("generate")){
            if (args.length != 2){
                usage();
                return;
            }
            WorldGenerator wg = new WorldGenerator();
            String tWorld = wg.generateRandomTounamentWorld();
            PrintWriter out;
            try{
                out = new PrintWriter(args[1]);
                out.print(tWorld);
                out.close();
            }catch (FileNotFoundException e){
                e.printStackTrace();
                return;}
            return;
        }else if (args[0].startsWith("-")){
            type = FunctionType.SINGLE;
            i = 0;
        }else{
            usage();
            return;
        }

        for (; i < args.length; ++i){
            if (args[i].equalsIgnoreCase("-nogui")){
                noGUI = true;
            }else if (args[i].equalsIgnoreCase("-br")){
                if (i == args.length-1){
                    usage();
                    return;
                }
                brainR = args[++i];
            }else if (args[i].equalsIgnoreCase("-bb")){
                if (i == args.length-1){
                    usage();
                    return;
                }
                brainB = args[++i];
            }else if (args[i].equalsIgnoreCase("-map")){
                if (i == args.length-1){
                    usage();
                    return;
                }
                map = args[++i];
            }else{
                usage();
                return;
            }
        }

        switch (type){
            case SINGLE:
                if (noGUI){
                    if (brainR == null || brainB == null){
                        usage();
                        break;
                    }
                    String result = runNoGUI(map, brainR, brainB);
                    System.out.println(result);
                }else
                    runGUI();
                break;
            case TOURNAMENT:
                if (brainR == null || brainB == null){
                    usage();
                    break;
                }
                String result = runNoGUI(map, brainR, brainB);
                System.out.println(result);
                result = runNoGUI(map, brainB, brainR);
                System.out.println(result);
                break;
            default:
                System.out.println("something impossible happened...");

        }
    }

    /**
     * prints the usage instructions to the console
     */
    public static void usage(){
        System.out.println("usage: [function] [options...]");
        System.out.println("functions are:");
        System.out.println("single\t run a single game");
        System.out.println("tournament\t run a tournament game always enable -nogui flag, so you must supply brains");
        System.out.println("leaving out function is the same as single");
        System.out.println("generate <file>\t generate a random world into the given file");
        System.out.println("options for single and tournament are:");
        System.out.println("-nogui\tdon't run gui, br and bb are mandatory with this option");
        System.out.println("-br <brain file>\tset the brain file for red ants");
        System.out.println("-bb <brain file>\tset the brain file for black ants");
        System.out.println("-map <world file>\tset the map");
    }

    /**
     * runs a single game from the command line
     * @param mapFile the map file to load
     * @param brainRFile the brain file for red ants
     * @param brainBFile the brain file for black ants
     * @return a string representing the result
     */
    public static String runNoGUI(String mapFile, String brainRFile, String brainBFile){
        GameHandler game = new GameHandler();
        try {
            game.setAntBrainRed(fileIO.parsers.BrainParser.ParseBrain(brainRFile));
        } catch (Exception e1) {
            System.out.println("Error parsing red brain" + e1.getMessage());
            return null;
        }
        try {
            game.setAntBrainBlack(fileIO.parsers.BrainParser.ParseBrain(brainBFile));
        } catch (Exception e1) {
            System.out.println("Error parsing black brain" + e1.getMessage());
            return null;
        }
        if(mapFile != null){
            try {
                game.setMap(fileIO.parsers.MapParser.parseMap(mapFile));
            } catch (Exception e1) {
                System.out.println("Error parsing map" + e1.getMessage());
                return null;
            }
        }
        game.startGame();
        while(!game.isFinished()){
            game.updateGame();
            System.out.printf("\rRound: %06d", game.round()); // due to an 11 (!!!!) year old bug, in eclipse console this does not work as intended.
        }                                                     // it works fine in any other console
        System.out.println();
        int scoreRed = game.getDisplayData().scoreRed;
        int scoreBlack = game.getDisplayData().scoreBlack;
        System.out.printf("Red scored:   %04d\n", scoreRed);
        System.out.printf("Black scored: %04d\n", scoreBlack);
        if (scoreRed == scoreBlack) return "It's a draw!";
        return (scoreRed > scoreBlack ? "Red" : "Black" )+" team won";
    }

    /**
     * runs the graphical user interface version
     */
    public static void runGUI(){
        // set look and feel to the system look and feel, because CrossPlatformLookAndFeel/Metal looks awful
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e){
            // oh well...
        }

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                Display disp = new Display();
                
                disp.setVisible(true);
            }
        });    
    }
}

