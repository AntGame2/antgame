package model.world;

import display.DisplayData;
import game.helper.Combat;
import game.helper.RNG;
import java.awt.Point;
import java.util.LinkedList;
import java.util.Random;
import model.ant.*;
import model.ant.Ant.AntColour;
import model.ant.Sense.condition;
import model.ant.Sense.senseDir;
import model.ant.Turn.direction;

/**
 * Handles the round data and the round logic
 * @author Alex
 */
public class World {
    //Map of the world
    private final Map map;
    //Some brains
    private final AntBrain brainRed;
    private final AntBrain brainBlack;
    //Shared data to publish changers to
    private final DisplayData dispData;
    //List of Ants
    private LinkedList<Ant> ants;    
    //RNG
    private final RNG rng;
    
    /**
     * Construct the final world object to be used
     * @param _map
     * @param _brainRed
     * @param _brainBlack 
     */
    public World(Map _map, AntBrain _brainRed, AntBrain _brainBlack) {
        map = _map;
        brainRed = _brainRed;
        brainBlack = _brainBlack;
        dispData = new DisplayData();
        ants = new LinkedList<>();
        rng = new RNG(new Random(System.currentTimeMillis()).nextInt());
        
        //Set up the ants ontop of their relevant bases
        int id = 0;
        for(int y = 0; y < map.getHeight(); y++){
            for(int x = 0; x < map.getWidth(); x++){
                switch(map.getCell(x, y).type){
                    case ANTHILL_RED:
                        //Ant(Brain, colour (false = red), id, Point)
                        ants.add(new Ant(brainRed, false, id, new Point(x, y)));
                        id++;
                        break;
                    case ANTHILL_BLACK:
                        //Ant(Brain, colour (true = black), id, Point)
                        ants.add(new Ant(brainBlack, true, id, new Point(x, y)));
                        id++;
                        break;                    
                }
            }
        } 
        //Publish the data for the display to use        
        dispData.setMap(map);
        dispData.setAnts(ants.toArray(new Ant[0]));
    }
    
    /**
     * Update all the ants for the next round
     */
    public void nextRound(){
        //Update all the ants
        for(Ant a : ants){
            if(!a.isAlive()){
                continue;
            }
            
            //Get the next instructuin
            Instruction inst = a.getInstruction();

            //Check if the ant is resting
            if(a.getResting() > 0){
                //Reduce resting
                a.setResting(a.getResting() - 1);
            //Process next instruction
            }else{
                //Sense a cell near the ant
                if(inst.getClass() == Sense.class){
                    Sense temp = (Sense) inst;
                    
                    if(cellMatches(a, temp.getCondVal(), temp.getSenseDirVal())){
                        a.setState(temp.getS1());
                    }else{
                        a.setState(temp.getS2());
                    }
                
                //Mark a cell with a pheremone
                }else if(inst.getClass() == Mark.class){
                    Mark temp = (Mark) inst;
                    setMarker(a.getPosition(), a.getColour(), temp.getMarker());
                    a.setState(temp.getS1());         
                    
                //Remove a mark from a cell
                }else if(inst.getClass() == Unmark.class){
                    Unmark temp = (Unmark) inst;
                    clearMarker(a.getPosition(), a.getColour(), temp.getMarker());
                    a.setState(temp.getS1());   
                    
                //Pick up food from the cell currently on
                }else if(inst.getClass() == PickUp.class){
                    PickUp temp = (PickUp) inst;
                    if(pickupFood(a.getPosition())){
                        a.setState(temp.getS1());
                    }else{
                        a.setState(temp.getS2());
                    }
                    
                //Drop food from the cell currently on
                }else if(inst.getClass() == Drop.class){
                    Drop temp = (Drop) inst;
                    dropFood(a.getPosition());
                    a.setState(temp.getS1());
                    
                //Turn the ant left or right
                }else if(inst.getClass() == Turn.class){
                    Turn temp = (Turn) inst;
                    a.setDirection(turn(a.getDirection(), temp.getTurnDir()));
                    a.setState(temp.getS1());
                    
                //Attempt to move to the cell infront of the ant
                }else if(inst.getClass() == Move.class){
                    Move temp = (Move) inst;
                    if(moveAnt(a)){
                        a.setState(temp.getS1());
                        a.setResting(14);
                        combatCheck(a);
                    }else{
                        a.setState(temp.getS2());
                    }
                    
                //Random choice
                }else if(inst.getClass() == Flip.class){
                    Flip temp = (Flip) inst;
                    
                    if(rng.randomint(temp.getRandom()) == 0){
                        a.setState(temp.getS1());
                    }else{
                        a.setState(temp.getS2());
                    }
                }
            }
        }
        
        //Publish data for the display
        dispData.setMap(map);
        dispData.setAnts(ants.toArray(new Ant[0]));
		
        int[] score = game.helper.Score.score(map);
		dispData.scoreRed = score[0];
		dispData.scoreBlack = score[1];
    }
    
    /**
     * Return the next direction the ant is facing
     * @param curr current direction
     * @param dir direction to turn
     * @return next direction facing
     */
    private int turn(int curr, direction dir){
        if(dir == direction.RIGHT){
            return (curr + 1) % 6;
        }else if(dir == direction.LEFT){
            if(curr == 0){
                return 5;
            }else{
                return (curr - 1) % 6;
            }
        }
        
        return curr;
    }
    
    /**
     * Put a marker at the cell selected
     * @param p the point of the cell
     * @param colour the colour of the ant
     * @param markerNum the number of the marker to set
     */
    private void setMarker(Point p, AntColour colour, int markerNum){
        map.getCell(p.x, p.y).set_marker_at(colour, markerNum);
    }
    
    /**
     * Remove a marker at the cell selected
     * @param p the point of the cell
     * @param colour the colour of the ant
     * @param markerNum the number of the marker to remove
     */
    private void clearMarker(Point p, AntColour colour, int markerNum){
        map.getCell(p.x, p.y).clear_marker_at(colour, markerNum);
    }
    
    /**
     * Check the ant provided, and all local and that surround it
     * @param target The at that was moved
     */
    private void combatCheck(Ant target){
        //Check if ant has put itself into death conditions
        singleCombatCheck(target);
        //Check if ant has put others into death conditions
        for(Point p : getSurroundingPoints(target.getPosition())){
            Ant temp = isAntAt(p);
            
            if(temp != null){
                singleCombatCheck(temp);
            }
        }    
    }
    
    /**
     * Check if a marker is present at the current location
     * @param a Ant that gave the check instruction
     * @param cond The condition to match
     * @param dir The direction of the sense
     * @return 
     */
    private boolean cellMatches(Ant a, condition cond, senseDir dir){
        Point pointToCheck;
        //Determine the cell to check
        switch(dir){
            case HERE:
                pointToCheck = a.getPosition();
                break;
                
            case AHEAD:
                pointToCheck = getSurroundingPoints(a.getPosition())[a.getDirection()];
                break;
                
            case LEFTAHEAD:
                pointToCheck = getSurroundingPoints(a.getPosition())[turn(a.getDirection(), direction.LEFT)];
                break;
                
            case RIGHTAHEAD:
                pointToCheck = getSurroundingPoints(a.getPosition())[turn(a.getDirection(), direction.RIGHT)];
                break;
                
            default:
                return false;
        }

        //Determine if the success condition is met
        switch(cond){
            case ROCK:
                return map.getCell(pointToCheck.x, pointToCheck.y).type == Cell.Type.ROCKY;
                
            case FRIEND: // isAntAt can return null causing NullPointerException so check first
                return isAntAt(pointToCheck) != null && isAntAt(pointToCheck).getColour() == a.getColour();
                
            case FOE: // isAntAt can return null causing NullPointerException so check first
                return isAntAt(pointToCheck) != null && isAntAt(pointToCheck).getColour() != a.getColour();
                
            case FRIENDWITHFOOD: // don't need to check if isAntAt returns null here as it would have been checked in the recursive call
                return cellMatches(a, condition.FRIEND, dir) && isAntAt(pointToCheck).hasFood();
                
            case FOEWITHFOOD:
                return cellMatches(a, condition.FOE, dir) && isAntAt(pointToCheck).hasFood();
                
            case FOOD:
                return map.getCell(pointToCheck.x, pointToCheck.y).foodCount() > 0;
                
            case HOME:
                if(a.getColour() == AntColour.BLACK){
                    return map.getCell(pointToCheck.x, pointToCheck.y).type == Cell.Type.ANTHILL_BLACK;
                }else{
                    return map.getCell(pointToCheck.x, pointToCheck.y).type == Cell.Type.ANTHILL_RED;
                }
            
            case FOEHOME:
                if(a.getColour() == AntColour.BLACK){
                    return map.getCell(pointToCheck.x, pointToCheck.y).type == Cell.Type.ANTHILL_RED;
                }else{
                    return map.getCell(pointToCheck.x, pointToCheck.y).type == Cell.Type.ANTHILL_BLACK;
                }
            
            case MARKER0:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_marker_at(a.getColour(), 0);

            case MARKER1:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_marker_at(a.getColour(), 1);
                
            case MARKER2:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_marker_at(a.getColour(), 2);
                
            case MARKER3:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_marker_at(a.getColour(), 3);
                
            case MARKER4:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_marker_at(a.getColour(), 4);
                
            case MARKER5:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_marker_at(a.getColour(), 5);
                
            case FOEMARKER:
                return map.getCell(pointToCheck.x, pointToCheck.y).check_foe_marker_at(a.getColour());
        }
      
        return false;
    }
    
    /**
     * Check if the ant provided has been put into death conditions
     * @param target The ant to check
     */
    private void singleCombatCheck(Ant target){
        //Check if ant has been put into death conditions
        if(Combat.checkCombat(target, ants)){
            Cell c = map.getCell(target.getPosition().x, target.getPosition().y);                      
                        
            if(target.hasFood()){
                c.putFood(4); // putFood *adds* food to the current amount, it does not replace it
            }else{
                c.putFood(3);
            }
            
            removeAnt(target);
        } 
    }
    
    /**
     * Return an array of points of all surrounding location, ordered as direction
     * E.g. [0] is east and [3] is west
     * @param centre The cell to find all surrounding points of
     * @return The surrounding points of the centre
     */
    private Point[] getSurroundingPoints(Point centre){
        Point[] possiblePoints = new Point[6];
        
        //East
        possiblePoints[0] = new Point(centre.x + 1, centre.y);
        
        //South-East
        if(centre.y % 2 > 0){
            possiblePoints[1] = new Point(centre.x + 1, centre.y + 1);
        }else{
            possiblePoints[1] = new Point(centre.x, centre.y + 1);
        }
        
        //South-West
        if(centre.y % 2 == 0){
            possiblePoints[2] = new Point(centre.x - 1, centre.y + 1);
        }else{
            possiblePoints[2] = new Point(centre.x, centre.y + 1);
        }
        
        //West
        possiblePoints[3] = new Point(centre.x - 1, centre.y);
        
        //North-West
        if(centre.y % 2 == 0){
            possiblePoints[4] = new Point(centre.x - 1, centre.y - 1);
        }else{
            possiblePoints[4] = new Point(centre.x, centre.y - 1);
        }
        
        //North-East
        if(centre.y % 2 > 0){
            possiblePoints[5] = new Point(centre.x + 1, centre.y - 1);
        }else{
            possiblePoints[5] = new Point(centre.x, centre.y - 1);
        }

        return possiblePoints;
    }
    
    /**
     * Pick up food from the cell provided
     * @param p The location to pick up food from
     * @return Whether food was picked up
     */
    private boolean pickupFood(Point p){
        Ant a = isAntAt(p);
        if (!a.hasFood()) //prevent ant from taking food if it already has one
            a.setFood(map.getCell(p.x, p.y).takeFood());
        return a.hasFood();
    }
    
    /**
     * Drop food from the cell provided
     * @param p The point to drop the food at
     */
    private void dropFood(Point p){
        Ant a = isAntAt(p);
        Cell c = map.getCell(p.x, p.y);
        
        if(a.hasFood()){
            a.setFood(false);
            c.putFood(1);
        }       
    }
    
    /**
     * Attempt to move an ant into the cell in front of it
     * @param ant The ant to move forward
     * @return Whether the ant was able to move forward
     */
    private boolean moveAnt(Ant ant){
        //Get relevant ant
        Point newPoint;
        Point[] possiblePoints = getSurroundingPoints(ant.getPosition());

        newPoint = possiblePoints[ant.getDirection()];
              
        if( (isAntAt(newPoint) != null) 
            || map.getCell(newPoint.x, newPoint.y).type == Cell.Type.ROCKY){
                return false;
        }
                
        ant.setPostition(newPoint);              
        return true;
    }

    /**
     * Determine if an ant is at a specific point
     * @param p The point to check
     * @return Return the ant at the location or null if not present
     */
    private Ant isAntAt(Point p){
        for(Ant a : ants){
            if(a.isAlive() && a.getPosition().equals(p)){
                return a;
            }
        }

        return null;
    }
    
    /**
     * Kill the ant provided
     * @param target The ant to kill
     */
    private void removeAnt(Ant target){
        target.kill();
    }
   
    /**
     * Return the shared data 
     * @return The shared data structure that describes ants and the cells
     */
    public DisplayData getDisplayData(){
        return dispData;
    }
}
