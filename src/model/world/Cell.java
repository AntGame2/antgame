package model.world;

import model.ant.Ant.AntColour;

public class Cell {

	public enum Type {
		CLEAR, ROCKY, ANTHILL_RED, ANTHILL_BLACK
	}

	public final Type type;

	private boolean[] redMarkers = new boolean[]{false, false, false, false, false, false};
	private boolean[] blackMarkers = new boolean[]{false, false, false, false, false, false};

	private int foodCount;

	public Cell(Type type) {
		this.type = type;
	}

	public Cell(Type type, int foodCount) {
		this.type = type;
		if (type != Type.ROCKY) this.foodCount = foodCount;
	}

	/**
	 * Remove one food from this cell
	 * if there is food in the cell, decrement the amount and return true
	 * otherwise return false
	 * For ant to use when told to pickup food
	 * 
	 * @return true if food was taken, false if not
	 */
	public boolean takeFood() {
		if (foodCount > 0) {
			--foodCount;
			return true;
		}
		return false;
	}

	/**
	 * Add the given amount of food to the cell
	 * For adding food when an ant dies from combat, or drops food
	 * 
	 * @param amount the amount of food to add, must be positive
	 */
	public void putFood(int amount) {
		if (amount > 0) {
			foodCount += amount;
		} else {
			// you can't add a non-positive amount of food
		}
	}

	/**
	 * Get the amount of food currently in the cell
	 * <p>
	 * For display and logging (dump file) purposes.
	 * 
	 * @return the current amount of food
	 */
	public int foodCount() {
		return foodCount;
	}

	/**
	 * put a black or red marker in the cell
     * 
     * @param colour the marker colour
     * @param markerNum the marker number
	 */
	public void set_marker_at(AntColour colour, int markerNum) {
	    if (markerNum >= 6 || markerNum < 0)
	        return;
		if (type == Type.CLEAR) {
			if (colour == AntColour.RED) {
				redMarkers[markerNum] = true;
			} else { // colour == BLACK
				blackMarkers[markerNum] = true;
			}
		}
	}

	/**
	 * clear a black or red marker in the cell
     * 
     * @param colour the marker colour
     * @param markerNum the marker number
	 */
	public void clear_marker_at(AntColour colour, int markerNum) {
        if (markerNum >= 6 || markerNum < 0)
            return;
		if (colour == AntColour.RED) {
            redMarkers[markerNum] = false;
		} else { // colour == BLACK
            blackMarkers[markerNum] = false;
		}
	}

	/**
	 * check is a marker inside a cell
	 * 
	 * @param colour the marker colour
	 * @param markerNum the marker number
	 * 
	 * @return true if the marker of the given colour is inside the cell,
	 *         otherwise false
	 * 
	 */
	public boolean check_marker_at(AntColour colour, int markerNum) {
        if (markerNum >= 6 || markerNum < 0)
            return false;
		if (colour == AntColour.RED) {
		    return redMarkers[markerNum];
		} else { // colour == BLACK
            return blackMarkers[markerNum];
		}
	}

	/**
	 * check is a marker of the opposite given colour inside a cell
	 * 
	 * @param colour the marker colour
	 * 
	 * @return true if any marker of the opposite given colour is inside the cell,
	 *         otherwise false
	 */
	public boolean check_foe_marker_at(AntColour colour) {
        if (colour == AntColour.BLACK) {
            for (boolean m : redMarkers){
                if(m) return true;
            }
        } else { // colour == RED
            for (boolean m : blackMarkers){
                if(m) return true;
            }
        }
        return false;
	}
}
