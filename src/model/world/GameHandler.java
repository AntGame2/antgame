package model.world;

import display.DisplayData;
import fileIO.parsers.ParserException;
import model.ant.AntBrain;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

/**
 * An instance of a game, handles all actions from start to finish
 * @author Alex
 */
public class GameHandler {
    private World world;
    private Map map;
    private AntBrain brainRed;
    private AntBrain brainBlack;
    private int currRound = 0;
    private int MAX_ROUND = 300000;    
    
    /**
     * Constructs the game based on the provided objects 
     */    
    public void startGame(){
        if(map == null){
            //Generate random map
            try{
                setMap(fileIO.parsers.MapParser.parseMap(
                        new BufferedReader(
                                new InputStreamReader(
                                        new ByteArrayInputStream(
                                                (new fileIO.generators.WorldGenerator())
                                                .generateRandomTounamentWorld().getBytes()
                        )))));
            }catch (IOException e){
                // This shouldn't happen
                e.printStackTrace();
            }catch (ParserException e){
                // This shouldn't happen either
                e.printStackTrace();
            }
        }
        //Start the game
        currRound = 0;
        world = new World(map, brainRed, brainBlack);
    }
    
    /**
     * Updates the game to the next round, 
     * Unless the maximum round is reached
     */
    public synchronized void updateGame() {
        if(currRound < MAX_ROUND){
            world.nextRound();
            currRound++;
        }  
    }
    
    /** 
     * Returns the  shared data to be used by Display.class
     * @return DisplayData 
     */
    public DisplayData getDisplayData(){
        return world.getDisplayData();
    }
    
    /**
     * Set the map to be used for the world
     * @param _map
     */
    public void setMap(Map _map){
        map = _map;
    }
    
    /**
     * Set the ant brain for the red ants
     * @param _brainRed 
     */
    public void setAntBrainRed(AntBrain _brainRed){
        brainRed = _brainRed;
    }
    
    /**
     * Set the ant brain for the black ants
     * @param _brainBlack 
     */
    public void setAntBrainBlack(AntBrain _brainBlack){
        brainBlack = _brainBlack;
    }
    
    /**
     * Set the maximum rounds of the simulation
     * @param maxRounds 
     */
    public void setMaxRounds(int maxRounds){
        MAX_ROUND = maxRounds;
    }

    /**
     * test whether the game has run to the maximum number of rounds
     * @return true if there are no more rounds to run
     * i.e. calling updateGame() does nothing otherwise false
     */
    public boolean isFinished(){
        return currRound >= MAX_ROUND;
    }

    /**
     * test whether the game has enough information to start the simulation
     * @return
     */
    public boolean canStart(){
        return brainRed != null && brainBlack != null;
    }
	
	 /**
     * get the current round
     * @return the current round number
     */
    public int round(){
        return currRound;
    }
}
