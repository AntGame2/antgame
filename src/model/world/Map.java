package model.world;

/**
 * Describes the map of the world and all cells within it
 * @author Alex
 */
public class Map {
    private final int WIDTH;
    private final int HEIGHT;
    private final Cell[][] cells;
    
    /**
     * Set up the map data-structure
     * @param width
     * @param height 
     */
    public Map(int width, int height) {
        WIDTH = width;
        HEIGHT = height;
        cells = new Cell[width][height];
    }
    
    /**
     * Place a cell at a given point of the map
     * @param x
     * @param y
     * @param c 
     */
    public void setCell(int x, int y, Cell c){
        cells[x][y] = c;
    }
       
    /**
     * Return the width of the map
     * @return 
     */
    public int getWidth(){
        return WIDTH;
    }
    
    /**
     * Return the height of the map
     * @return 
     */
    public int getHeight(){
        return HEIGHT;
    }
    
    /**
     * Get a cell at a given point
     * @param x
     * @param y
     * @return 
     */
    public Cell getCell(int x, int y){
        //Check if the cell is out of bounds
        if(x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT){
            return new Cell(Cell.Type.ROCKY);
        }
        
        return cells[x][y];
    }
}