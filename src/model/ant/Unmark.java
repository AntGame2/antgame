
package model.ant;

/**
 *
 * @author User
 */
public class Unmark extends Instruction {

    private int state;
    private int marker;

    /**
     * Unmark Instruction takes a marker number and the next state.
     * @param state the next state after the unmark
     * @param marker the marker number to remove
     */
    public Unmark(int marker, int state) {
        this.state = state;
        this.marker = marker;
    }

    /**
     * Get the next state after a marker is removed.
     * @return next state
     */
    public int getS1() {
        return this.state;
    }

    /**
     * Get the marker number to remove.
     * @return marker number to remove
     */
    public int getMarker() {
        return this.marker;
    }
}
