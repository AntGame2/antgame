package model.ant;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public final class AntBrain {

    private ArrayList<Instruction> fsm = new ArrayList<>();
    private final String name;


    public AntBrain(String _name, ArrayList<Instruction> _fsm){
        name = _name;
        fsm = _fsm;
    }

    /**
     * Returns the Instruction within the ArrayList of Instructions at the specified index, where index == state number.
     * @param currState the next state number
     * @return Instruction at the specified state
     */
    public Instruction getInstruction(int currState) {
        return fsm.get(currState);
    }

     /**
     * Returns the name of this AntBrain (the original file name of the brain file).
     * @return the name of this AntBrain 
     */
    public String getName() {
        return name;
    }
}
