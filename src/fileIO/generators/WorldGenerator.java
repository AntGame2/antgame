package fileIO.generators;
import java.awt.Point;
//import java.io.FileNotFoundException;
//import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;


/**
 * This program is poorly written and hard to understand, but it works.
 * Due to the use of random inside do while loops, their is no guarantee that this program will ever exit,
 * though this eventuality is extremely improbable
 * @author Michael Sledge
 *
 */
public class WorldGenerator {

    public static final int TOURNAMENT_WIDTH        = 150;
    public static final int TOURNAMENT_HEIGHT       = 150;
    public static final int TOURNAMENT_ROCK_NUM     = 14;
    public static final int TOURNAMENT_FOODBLOB_NUM = 11;
    public static final int TOURNAMENT_HILL_RADIUS  = 7;
    
    Random r = new Random(System.currentTimeMillis());
    
    private enum cellType {
        CLEAR('.'),
        ROCKY('#'),
        RED_HILL('+'),
        BLACK_HILL('-');
        final char cellChar;
        cellType(char cellChar){
            this.cellChar = cellChar;
        }
    };

    /**
     * generate a random cell, used by generateRandomWorld
     * @return a random character representing a cell type
     */
    private cellType randomCell(){
        int c = r.nextInt(400);
        if (c >= 30) return cellType.CLEAR;
        return cellType.values()[c/10 + 1];
    }

    /**
     * Generates an entirely random world with the given dimensions
     * This method gives no guarantees on composition of the world (it may have a different number of cells of each anthill colour, or even none at all)
     * @param width width of the generated world
     * @param height height of the generated world
     * @return A String representing the generated world
     */
    public String generateRandomWorld(int width, int height){
        StringBuilder worldString = new StringBuilder();
        worldString.append(width + "\n");
        worldString.append(height + "\n");
        for (int i = 0; i < width; ++i){
            for (int j = 0; j < height; ++j){
                char cChar = randomCell().cellChar;
                if (cChar == '.' && r.nextInt(20) == 0){
                    cChar = (""+(1+r.nextInt(9))).toCharArray()[0];
                }
                worldString.append(cChar);
                worldString.append(" ");
            }
            worldString.append("\n");
        }
        return worldString.toString();
    }
    
    private boolean hillsOverlap(Point hill1, Point hill2){
        int xdiff = Math.abs(hill1.x - hill2.x);
        int ydiff = Math.abs(hill1.y - hill2.y);
        return (xdiff + ydiff) < TOURNAMENT_HILL_RADIUS * 2;
    }
    
    /**
     * calculates the coordinates of all cells in a tournament sized anthill (side length 7) given its centre point
     * @param h centre point of hill
     * @return array of all points in a tournament anthill centred on h
     */
    private Point[] hillPoints(Point h){
        ArrayList<Point> points = new ArrayList<>();
        
        // ridiculous way of calculating subhex positions
        // top half
        for(int y = 6; y > 0; --y){
            for (int x = 0; x < 13 - y; ++x){
                points.add(new Point((h.x-6) + (y+(h.y%2))/2 + x,h.y - y));
            }
        }
        // middle line
        for(int i = -6; i < 7; ++i){
            points.add(new Point(h.x + i,h.y));
        }
        // bottom half
        for(int y = 1; y <= 6; ++y){
            for (int x = 0; x < 13 - y; ++x){
                points.add(new Point((h.x-6) + (y+(h.y%2))/2 + x,h.y + y));
            }
        }
        
        return points.toArray(new Point[0]);
    }
    
    /**
     * Checks if a food blob placed at topleft might overlap rocks, anthills or other food blobs.
     * may return true even when the blob doesn't overlap, only returns false when it is guaranteed not to overlap
     * @param topleft top left corner of hypothetical food blob
     * @param map the map the food blob could be placed on
     * @return true if the food blob may overlap with another feature
     */
    private boolean foodBlobOverlaps(Point topleft, char[][] map){
        for (int x = topleft.x - 2; x < topleft.x + 7; ++x){
            for (int y = topleft.y; y < topleft.y + 5; ++y){
                if (map[x][y] != '.') return true;
            }
        }
        return false;
    }

    /**
     * Generates a world which conforms to the tournament rules.
     * rules are as follows:
     * 150 * 150 dimensions,
     * rocks around all edges,
     * 1 hexagon of side seven for of each colour of anthill,
     * 11 5x5 food blobs,
     * 14 rocks (not around the edge)
     * @return A String representing the generated world
     */
    public String generateRandomTounamentWorld(){

        int width = TOURNAMENT_WIDTH;
        int height = TOURNAMENT_HEIGHT;
        Point redHill, blackHill;
        
        char[][] map = new char[width][height];
        
        redHill = new Point(r.nextInt(width - (1 + 2*TOURNAMENT_HILL_RADIUS)) + TOURNAMENT_HILL_RADIUS,
                r.nextInt(height - (1 + 2*TOURNAMENT_HILL_RADIUS)) + TOURNAMENT_HILL_RADIUS);
        
        do{
            blackHill = new Point(r.nextInt(width - (1 + 2*TOURNAMENT_HILL_RADIUS)) + TOURNAMENT_HILL_RADIUS,
                r.nextInt(height - (1 + 2*TOURNAMENT_HILL_RADIUS)) + TOURNAMENT_HILL_RADIUS);
        }while(hillsOverlap(redHill, blackHill));

        for (int i = 0; i < width; ++i){
            map[i][0] = '#';
        }

        for (int j = 1; j < height - 1; ++j){
            map[0][j] = '#';
            for (int i = 1; i < width - 1; ++i){
                map[i][j] = '.';
            }
            map[width-1][j] = '#';
        }

        for (int i = 0; i < width; ++i){
            map[i][height - 1] = '#';
        }

        for (Point p : hillPoints(redHill)){
            map[p.x][p.y] = '+';
        }

        for (Point p : hillPoints(blackHill)){
            map[p.x][p.y] = '-';
        }

        for (int i = 0; i < TOURNAMENT_FOODBLOB_NUM; ++i){
            Point fbp = new Point();
            do{
                fbp.x = r.nextInt(width - (7+3)) + 3;
                fbp.y = r.nextInt(height - 6) + 1; 
            }while(foodBlobOverlaps(fbp, map));
            boolean mirrored = r.nextBoolean();
            for (int x = 0; x < 5; ++x){
                for (int y = 0; y < 5; ++y){
                    if (!mirrored)
                        map[fbp.x+x + (y+fbp.y%2)/2][fbp.y+y] = '5';
                    else
                        map[fbp.x+x - (y+1+fbp.y%2)/2][fbp.y+y] = '5';
                }
            }
        }
        
        for (int i = 0; i < TOURNAMENT_ROCK_NUM; ++i){
            Point rp = new Point();
            do{
                rp.x = r.nextInt(width - 2) + 1; 
                rp.y = r.nextInt(height - 2) + 1; 
            }while(map[rp.x][rp.y] != '.');
            map[rp.x][rp.y] = '#';
        }

        StringBuilder worldString = new StringBuilder();
        worldString.append(width + "\n");
        worldString.append(height + "\n");
        for (int j = 0; j < height; ++j){
            for (int i = 0; i < width; ++i){
                worldString.append(map[i][j]);
                worldString.append(" ");
            }
            worldString.append("\n");
        }
        return worldString.toString();
    }

//    private static void usage(){
//        System.out.println("usage: <tournament|random> <outputfile> [width] [height]");
//        System.out.println("[width] and [height] are only used if the first argument is random");
//    }
//
//    public static void main(String[] args){
//        
//        if (args.length < 2){
//            usage();
//            return;
//        }
//
//        if (args[0].equalsIgnoreCase("tournament")){
//            WorldGenerator wg = new WorldGenerator();
//            String tWorld = wg.generateRandomTounamentWorld();
//            PrintWriter out;
//            try{
//                out = new PrintWriter(args[1]);
//                out.print(tWorld);
//                out.close();
//            }catch (FileNotFoundException e){
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//                return;
//            }
//        }else if (args[0].equalsIgnoreCase("random")){
//            if (args.length < 4){
//                usage();
//                return;
//            }
//            WorldGenerator wg = new WorldGenerator();
//            int width;
//            int height;
//            try{
//                width = Integer.parseInt(args[2],10);
//                height = Integer.parseInt(args[3], 10);
//            }catch(NumberFormatException e){
//                usage();
//                return;
//            }
//            String tWorld = wg.generateRandomWorld(width, height);
//            PrintWriter out;
//            try{
//                out = new PrintWriter(args[1]);
//                out.print(tWorld);
//                out.close();
//            }catch (FileNotFoundException e){
//                e.printStackTrace();
//                return;
//            }
//        }else{
//            usage();
//        }
//    }
}
