package fileIO.parsers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.ant.AntBrain;
import model.ant.Drop;
import model.ant.Flip;
import model.ant.Instruction;
import model.ant.Mark;
import model.ant.Move;
import model.ant.PickUp;
import model.ant.Sense;
import model.ant.Turn;
import model.ant.Unmark;


public class BrainParser {

    /**
     * Constructor initialises an AntBrain from a given filename located within
     * the root folder; it then proceeds to check the syntax of the file before
     * reading it line by line and translating it into Instructions, these
     * instructions are then added to an ArrayList of Instructions that
     * represent the whole AntBrain.
     * 
     * @param antBrainFile
     *            filename of the AntBrain to load
     * @return
     * @throws java.io.IOException
     * @throws fileIO.parsers.ParserException
     */
    public static AntBrain ParseBrain(String antBrainFile) throws IOException, ParserException {
        String name = antBrainFile;
        ArrayList<Instruction> fsm = new ArrayList<>();
        try {
            // First check the syntax of the brain file is correct, if not throw an error.
            if (checkAntBrainSyntax(antBrainFile)) {
                BufferedReader br = new BufferedReader(new FileReader(antBrainFile));
                String currLine;
                while ((currLine = br.readLine()) != null) {
                    currLine = currLine.toLowerCase();
                    String[] input = currLine.split("\\s");
                    Instruction i = null;
                    // Switch-case based upon first string in the current line
                    switch (input[0]) {
                        case "sense":
                            String markerNum = "";
                            if (input.length >= 6) {
                                String regexMarker = "[0-5]";
                                Pattern p1 = Pattern.compile(regexMarker);
                                Matcher m = p1.matcher(input[5]);
                                if (m.matches()) {
                                    markerNum = input[5];
                                }
                            }
                            String cond = input[4] + markerNum;
                            i = new Sense(Sense.dirFromString(input[1]), Integer.parseInt(input[2]), Integer.parseInt(input[3]), Sense.condFromString(cond));
                            break;
                        case "mark":
                            i = new Mark(Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                            break;
                        case "unmark":
                            i = new Unmark(Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                            break;
                        case "pickup":
                            i = new PickUp(Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                            break;
                        case "drop":
                            i = new Drop(Integer.parseInt(input[1]));
                            break;                                             
                        case "turn":
                            i = new Turn(Turn.dirFromString(input[1]), Integer.parseInt(input[2]));
                            break;
                        case "move":
                            i = new Move(Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                            break;
                        case "flip":
                            i = new Flip(Integer.parseInt(input[1]), Integer.parseInt(input[2]), Integer.parseInt(input[3]));
                            break;
                    }
                    fsm.add(i);
                }
                return new AntBrain(name, fsm);

            } else {
                // Exception should be thrown by checkAntBrainSyntax by this point
                throw new ParserException("Error in ant brain file");
            }
        } catch (ParserException | IOException | NumberFormatException e) {
            throw new ParserException("Error: " + e.getMessage());
        }
    }

    /**
     * Check if ant brain file is syntactically correct iterates through passed
     * file line by line
     *
     * @param antBrainFile the ant brain file (.brain) to check
     * @return boolean true if ant brain is syntactically correct false if not
     * @throws fileIO.parsers.ParserException
     * @throws java.io.FileNotFoundException
     */
    public static boolean checkAntBrainSyntax(String antBrainFile) throws ParserException, FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(antBrainFile));
        String currLine;
        int lineNumber = 0;

        // process each line in ant brain file
        while ((currLine = br.readLine()) != null) {

            lineNumber++;
            // convert current line to lowercase
            currLine = currLine.toLowerCase();

            // store variables used for regular expression
            String st = "[0-9][0-9]{0,3}";
            String i = "[0-5]";
            String comment = "(\\s*?;.*$)?"; // ignore everything after ;

            // check Sense
            String regex = "((sense)*\\s(here|ahead|leftahead|rightahead)\\s(" + st + ")\\s(" + st + ")\\s(friend|foe|friendwithfood|foewithfood|food|rock|(marker\\s" + i + ")|foemarker|home|foehome))" + comment;

            // check Mark and Unmark
            regex += "|((mark|unmark)*\\s(" + i + ")\\s(" + st + "))" + comment;

            // check PickUp and Move
            regex += "|((pickup|move)*\\s(" + st + ")\\s(" + st + "))" + comment;

            // check Drop
            regex += "|((drop)*\\s(" + st + "))" + comment;

            // check Turn
            regex += "|((turn)*\\s(left|right)\\s(" + st + "))" + comment;

            // check Flip
            regex += "|((flip)*\\s([0-9]*)\\s(" + st + ")\\s(" + st + "))" + comment;

            // perform regular expression
            Pattern p1 = Pattern.compile(regex);
            Matcher m = p1.matcher(currLine);

            // if current line doesn't match regular expression, print error
            // to terminal and return false (could possibly throw exception here?)
            if (!m.matches()) {
                //System.out.println("Error in java brain file "+file+" at line "+lineNumber+ ": "+currLine);
                throw new ParserException("Error in java brain file " + antBrainFile + " at line " + lineNumber + ": " + currLine);
                //return false;
            }
        }
        return true;
    }

}
