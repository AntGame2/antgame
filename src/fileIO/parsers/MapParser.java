package fileIO.parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import model.world.Map;
import model.world.Cell;

/**
 * This class contains methods for converting a file containing a map int a map object
 * @author Michael Sledge
 *
 */
public class MapParser{

    /**
     * Loads a file and produces a map from it
     * @param fileName name (including absolute or relative path) of the file to parse
     * @return a map matching the representation in the given file
     * @throws IOException Error reading file
     * @throws ParserException file did not contain a valid representation of a map
     */
    public static Map parseMap(String fileName) throws IOException, ParserException{
        File worldFile = new File(fileName);
        BufferedReader br = new BufferedReader(new FileReader(worldFile));
        return parseMap(br);
    }

    /**
     * Loads a file and produces a map from it
     * @param file a File object representing the file to parse and convert into a map object
     * @return a map matching the representation in the given file
     * @throws IOException Error reading file
     * @throws ParserException file did not contain a valid representation of a map
     */
    public static Map parseMap(File file) throws IOException, ParserException{
        BufferedReader br = new BufferedReader(new FileReader(file));
        return parseMap(br);
    }

    /**
     * Produces a map from the representation from a BufferedReader
     * @param br a BufferedReader reading from a textual representation of a map
     * @return a map matching the representation in the given file
     * @throws IOException Error reading file
     * @throws ParserException file did not contain a valid representation of a map
     */
    public static Map parseMap(BufferedReader br) throws IOException, ParserException{
        final int width;
        final int height;
        String line = "";
        try{
            line = br.readLine();
            width = Integer.parseInt(line, 10);
        }catch (NumberFormatException e){
            throw new ParserException("Invalid value for width, line 1: " + line);
        }

        try{
            line = br.readLine();
            height = Integer.parseInt(line, 10);
        }catch (NumberFormatException e){
            throw new ParserException("Invalid value for height, line 2: " + line);
        }

        Map map = new Map(width, height);

        for (int y = 0; y < height; ++y){
            line = br.readLine();
            if (line == null){
                // end of stream
                throw new ParserException(
                        "reached end of file unexpectedly after line " + (y+2) +
                        ", expected " + (height + 2) + " lines");
            }
            parseLine(line, map, width, y);
        }

        return map;

    }

    /**
     * Parses a single line, sets the cells of the given world
     * to match the info in the line
     * @param line the string to be parsed
     * @param map the map object to set the cells of
     * @param width width of the world, expected number of
     * non-whitespace characters in line
     * @param lineNumber the number of the line given,
     * the vertical coordinate of the cells to be set
     * @throws ParserException if the line is not the expected length or
     * an invalid character is encountered
     */
    private static void parseLine(String line, Map map, int width, int lineNumber) throws ParserException{
        int column = 0;
        for(char ch : line.toCharArray()){
            switch (ch){
                case '#': // rock
                    map.setCell(column, lineNumber, new Cell(Cell.Type.ROCKY));
                    ++column;
                    break;
                case '+': // red hill
                    map.setCell(column, lineNumber, new Cell(Cell.Type.ANTHILL_RED));
                    ++column;
                    break;
                case '-': // black hill
                    map.setCell(column, lineNumber, new Cell(Cell.Type.ANTHILL_BLACK));
                    ++column;
                    break;
                case '.': // clear
                    map.setCell(column, lineNumber, new Cell(Cell.Type.CLEAR));
                    ++column;
                    break;
                case '1': // clear with 1 food
                case '2': // clear with 2 food
                case '3': // clear with 3 food
                case '4': // clear with 4 food
                case '5': // clear with 5 food
                case '6': // clear with 6 food
                case '7': // clear with 7 food
                case '8': // clear with 8 food
                case '9': // clear with 9 food
                    map.setCell(column, lineNumber, new Cell(Cell.Type.CLEAR,
                            Integer.parseUnsignedInt(""+ch, 10)));
                            // if the line above throws a NumberFormatException,
                            // something is very wrong.
                    ++column;
                    break;
                case ' ': // spacing between chars to make human it readable
                    break;
                default: // unknown character
                    throw new ParserException("Unkown character '" + ch + "' at " +
                            "line " + (lineNumber + 2));

            }
        }
    }

}
