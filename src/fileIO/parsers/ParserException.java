package fileIO.parsers;

/**
 * Exception for use by parsers to indicate errors while parsing
 * @author Michael Sledge
 *
 */
@SuppressWarnings("serial")
public class ParserException extends Exception {
    public ParserException(String message) {
        super(message);
    }

}
