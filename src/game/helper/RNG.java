package game.helper;

import static java.lang.Math.abs;

public class RNG {
	private final int seed;
	private int s;

	public RNG(int initialSeed) {
		this.seed = initialSeed;
		for (int i = 0; i < 4; i++) {
			s = s * 22695477 + 1;
		}
	}
	
	/**
	 * generate a pseudo-random integer between 0 and n-1
	 * 
	 * @param a positive number for calculating the pseudo-random number
	 * @return a pseudo-random integer 
	 */
	public int randomint(int n) {
		int x = (s / 65536) % 16384;
		s = s * 22695477 + 1;

		// convert x into unsigned int then mod n
                //abs(x) is the same as the mathematical |x|
		return (abs(x) % n);
	}
	
	/**
	 * reset the random seed
	 */
	public void resetSeed() {
		this.s = seed;
		for (int i = 0; i < 4; i++) {
			s = s * 22695477 + 1;
		}
	}
}
