package game.helper;

import model.world.Cell;
import model.world.Map;


public class Score {
    
    /**
     * calculates the score (amount of food on the anthill cells) for each team
     * @param map the map to score on
     * @return an integer array of length 2 containing red ant score and black ant score in indexes 0 and 1 respectively
     */
    public static int[] score(Map map){
        int[] score = {0,0};
        for (int x = 0; x < map.getWidth(); ++x){
            for (int y = 0; y < map.getHeight(); ++y){
                Cell c = map.getCell(x, y);
                if (c.type == Cell.Type.ANTHILL_RED){
                    score[0] += c.foodCount();
                }else if (c.type == Cell.Type.ANTHILL_BLACK){
                    score[1] += c.foodCount();
                }
            }
        }
        return score;
    }
}
