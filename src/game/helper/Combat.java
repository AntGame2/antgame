package game.helper;


import java.awt.Point;
import java.util.LinkedList;
import model.ant.Ant;

public class Combat {

	/**
	 * check is an ant surrounded by 5 or more enemy,
	 * 
         * @param target
         * @param ants list of ant
	 * @return ants which are alive
	 */
	public static boolean checkCombat(Ant target, LinkedList<Ant> ants) {
		Ant ant = target;
		int count = 0;

		for (int j = 0; j < ants.size(); j++) {
			Ant temp = ants.get(j);
                        
                        if(!temp.isAlive()){
                            continue;
                        }
		
			// if the ant's y is even num
			if (ant.getPosition().y % 2 == 0) {
				// direction 0
				if (ant.getPosition().x + 1 == temp.getPosition().x && ant.getPosition().y == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 1
				if (ant.getPosition().x == temp.getPosition().x && ant.getPosition().y + 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 2
				if (ant.getPosition().x - 1 == temp.getPosition().x && ant.getPosition().y + 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 3
				if (ant.getPosition().x - 1 == temp.getPosition().x && ant.getPosition().y == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 4
				if (ant.getPosition().x - 1 == temp.getPosition().x && ant.getPosition().y - 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 5
				if (ant.getPosition().x == temp.getPosition().x && ant.getPosition().y - 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
			// if the ant's y is odd num
			}else {
				// direction 0
				if (ant.getPosition().x + 1 == temp.getPosition().x && ant.getPosition().y == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 1
				if (ant.getPosition().x + 1 == temp.getPosition().x && ant.getPosition().y + 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 2
				if (ant.getPosition().x == temp.getPosition().x && ant.getPosition().y + 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 3
				if (ant.getPosition().x - 1 == temp.getPosition().x && ant.getPosition().y == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 4
				if (ant.getPosition().x == temp.getPosition().x && ant.getPosition().y - 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
				// direction 5
				if (ant.getPosition().x + 1 == temp.getPosition().x && ant.getPosition().y - 1 == temp.getPosition().y && temp.getColour() != ant.getColour()) count++;
			}
		}
		
		if (count >= 5) {
                        return true;
		}else{
                        return false;
                }
    }
}
